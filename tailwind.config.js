/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme');

export default {
    content: [
        './index.html',
        './src/**/*.{vue,js,ts,jsx,tsx}'
    ],
    theme: {
        extend: {
            colors: {
                gradientFrom: '#ff80b5',
                gradientTo: '#9089fc',
                //gradientFrom: '#ffaf40',
                // gradientTo: '#ff6363',
                light: '#213547',
                black: '#1e1e1e',
                success: '#38b000',
                'success-light': '#b9fbc0',
                warning: '#f08c00',
                'warning-light': '#ffe5b4',
                error: '#d00000',
                'error-light': '#ffb3b3',
                'accent-blue': '#BBDEFB',
                'accent-purple': '#a29bfe',
                'accent-pink': '#FFCDD2',
                'accent-green': '#C8E6C9',
                projects: {
                    'F87171': '#F87171',
                    '60A5FA': '#60A5FA',
                    '34D399': '#34D399',
                    'FBBF24': '#FBBF24',
                    'A78BFA': '#A78BFA',
                    'E879F9': '#E879F9',
                    '93C5FD': '#93C5FD',
                    'FCD34D': '#FCD34D',
                    '86EFAC': '#86EFAC',
                    '22D3EE': '#22D3EE'
                }
            },
            animation: {
                'ping-slow': 'ping 3s cubic-bezier(0, 0, 0.2, 1) infinite',
                'spin-slow': 'spin 5s linear infinite',
                'spin-alternate': 'spin 1s linear infinite'
            },
            fontFamily: {
                fontFamily: {
                    base: ['Inter', ...defaultTheme.fontFamily.sans],
                    brand: ['Alexandria', ...defaultTheme.fontFamily.sans]
                },
                fontSize: {
                    base: '1rem',
                    h1: '2.25rem',
                    h2: '1.875rem',
                    h3: '1.5rem',
                    h4: '1.25rem',
                    h5: '1.125rem',
                    h6: '1rem'
                },
                lineHeight: {
                    h1: '1.3',
                    h2: '1.35',
                    h3: '1.4',
                    h4: '1.45',
                    h5: '1.5',
                    h6: '1.5'
                },
                fontWeight: {
                    normal: 400,
                    medium: 500,
                    bold: 600,
                    extrabold: 700
                },
                colors: {
                    text: {
                        light: '#213547',
                        black: '#1e1e1e'
                    },
                    background: {
                        light: '#ffffff',
                        dark: '#f4f4f6'
                    },
                    link: {
                        light: '#646cff',
                        hover: '#535bf2'
                    },
                    button: {
                        light: '#f9f9f9',
                        dark: '#d1d5db',
                        primary: '#4c6ef5'
                    },
                    border: {
                        light: '#e5e5e5',
                        dark: '#cbd5e1'
                    },
                    success: '#38b000',
                    'success-light': '#b9fbc0',
                    warning: '#f08c00',
                    'warning-light': '#ffe5b4',
                    error: '#d00000',
                    'error-light': '#ffb3b3',
                    'accent-blue': '#3a8bfd',
                    'accent-purple': '#a29bfe',
                    'accent-green': '#34d399'
                },
                borderRadius: {
                    small: '8px'
                },
                spacing: {
                    small: '0.6em',
                    medium: '1.2em'
                }
            }
        }
    },
    plugins: [
        require('@tailwindcss/typography'),
        require('@tailwindcss/forms'),
        require('@tailwindcss/aspect-ratio'),
        require('@tailwindcss/container-queries')
    ],
    safelist: [
        'text-projects-F87171',
        'text-projects-60A5FA',
        'text-projects-34D399',
        'text-projects-FBBF24',
        'text-projects-A78BFA',
        'text-projects-E879F9',
        'text-projects-93C5FD',
        'text-projects-FCD34D',
        'text-projects-86EFAC',
        'text-projects-22D3EE',

        'bg-projects-F87171',
        'bg-projects-60A5FA',
        'bg-projects-34D399',
        'bg-projects-FBBF24',
        'bg-projects-A78BFA',
        'bg-projects-E879F9',
        'bg-projects-93C5FD',
        'bg-projects-FCD34D',
        'bg-projects-86EFAC',
        'bg-projects-22D3EE',

        'group-hover:bg-projects-F87171',
        'group-hover:bg-projects-60A5FA',
        'group-hover:bg-projects-34D399',
        'group-hover:bg-projects-FBBF24',
        'group-hover:bg-projects-A78BFA',
        'group-hover:bg-projects-E879F9',
        'group-hover:bg-projects-93C5FD',
        'group-hover:bg-projects-FCD34D',
        'group-hover:bg-projects-86EFAC',
        'group-hover:bg-projects-22D3EE'
    ]
};

