# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

### Changed

### Removed

## [0.3.1] - 2024-12-15

### Fixed

- Hardcoded user id when creating ticket from gitlab issue
- Missing translation in english file

## [0.3.0] - 2024-12-15

### Added

- Login api link
- Basic session management
- Register form api link
- Basic profile avatar generator
- Project views (view, edit)
- Sprint view (view, edit, create)
- Project sprints' view (view all project sprints)
- Sprints' tickets' view (view and manage tickets of a sprint)
- Sync Gitlab issues
- Create ticket for a project from a Gitlab issue
- Distant repository sync (merge requests, issues, branches, commits)
- Breadcrumb for navigation in pages
- Project backlog

### Changed

- refactored useForm to extend the use to generic forms
- register form validation
- reverted routes depth change to base URL /
- useAuth to implement api link
- top right login header (api link)
- pre-load tailwind classes
- update lang files (en/fr)

### Removed

- organizations from side bar

## [0.2.0] - 2024-12-02

### Added

- Teams creation / assignement form
- Project creation form
- Gitlab / Github project integration / sync
- Repositories visualization (MR, issues, activity feed)
- Board component (Columns with status, drag and drop)
- Tickets forms (Update / create)
- Tickets components (Labels, priority)

## [0.1.0] - 2024-11-18

### Added

- Project setup (Vue3, Tailwind)
- Initialized lefthook
- Initialized eslint
- Initialized prettier
- Error pages
- layouts(sidebar, default, auth)
- User form (login / registration / edition)
- router (vue-router)
- Basic mapping models
- OSEF API integration
- custom i18n
- french / english translations
- Gitlab CI

[unreleased]: https://gitlab.emi.u-bordeaux.fr/conduite-de-projet/osef/osef-webapp

[0.3.1]: https://gitlab.emi.u-bordeaux.fr/conduite-de-projet/osef/osef-webapp/tags/0.3.1

[0.3.0]: https://gitlab.emi.u-bordeaux.fr/conduite-de-projet/osef/osef-webapp/tags/0.3.0

[0.2.0]: https://gitlab.emi.u-bordeaux.fr/conduite-de-projet/osef/osef-webapp/tags/0.2.0

[0.1.0]: https://gitlab.emi.u-bordeaux.fr/conduite-de-projet/osef/osef-webapp/tags/0.1.0
