<template>
    <Stepper :steps="steps" @move-to="moveTo" />
    <form-error
        v-if="errors.length"
        id="form-errors"
        :errors="errors"
        class="my-6"
    />
    <component
        :is="activeStep?.component"
        :form="form"
        :step="activeStep"
        class="max-h-full mt-6 p-3"
    />
    <div
        class="fixed bottom-0 inset-x-0 p-8 flex justify-end bg-white backdrop-blur bg-opacity-30"
    >
        <button
            v-if="!isCompleted"
            class="primary inline-flex items-center rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
            type="button"
            @click="isLastStepCurrentStep ? submit() : nextStep()"
        >
            {{
                $text(
                    isLastStepCurrentStep
                        ? 'pages.projectPage.create.finishCreation'
                        : 'pages.projectPage.create.nextStep',
                )
            }}
        </button>
    </div>
</template>

<script lang="ts" setup>
import { Step } from '@/assets/types/components/Step';
import Stepper from '@/components/inputs/Stepper.vue';
import { StepStatus } from '@/assets/types/helpers/enums/StepStatus.ts';
import Step1 from '@/components/pages/forms/ProjectCreationFormSteps/Step1.vue';
import Step4 from '@/components/pages/forms/ProjectCreationFormSteps/Step4.vue';
import Step2 from '@/components/pages/forms/ProjectCreationFormSteps/Step2.vue';
import Step3 from '@/components/pages/forms/ProjectCreationFormSteps/Step3.vue';
import { computed, ComputedGetter, nextTick, ref, Ref, shallowRef } from 'vue';
import { ProjectStatus } from '@/assets/types/models/enums/ProjectStatus';
import { ProjectVisibility } from '@/assets/types/models/enums/ProjectVisibility';
import { Sprint } from '@/assets/types/models/Sprint';
import { Ticket } from '@/assets/types/models/Ticket';
import { Repository } from '@/assets/types/models/Repository';
import { Required } from '@/assets/types/components/inputRules/Required';
import { useText } from '@/assets/composables/useText';
import { useForm } from '@/assets/composables/useForm';
import FormError from '@/components/errors/FormError.vue';
import { FutureDate } from '@/assets/types/components/inputRules/FutureDate';
import { IsNumber } from '@/assets/types/components/inputRules/IsNumber';
import { toast } from 'vue3-toastify';
import useRepository from '@/assets/composables/useRepository';
import { Project } from '@/assets/types/models/Project';
import { useRouter } from 'vue-router';
import useAuth from '@/assets/composables/useAuth';

const { text } = useText();
const { validate } = useForm();
const projectRepository = useRepository<Project>('projects');

const steps: Ref<Step[]> = ref([
    {
        id: 'Step 1',
        name: 'Project details',
        show: true,
        validated: false,
        skipValidation: false,
        status: StepStatus.CURRENT,
        component: shallowRef(Step1),
        fields: {
            name: { label: 'projectName', type: 'text', rules: [Required] },
            description: {
                label: 'projectDescription',
                type: 'text',
                rules: [],
            },
            startDate: { label: 'startDate', type: 'date', rules: [Required] },
            endDate: { label: 'endDate', type: 'date' },
            icon: { label: 'projectIcon', rules: [Required] },
            visibility: {
                label: 'projectVisibility',
                options: [
                    {
                        label: text(`common.${ProjectVisibility.Public}`),
                        value: ProjectVisibility.Public,
                    },
                    {
                        label: text(`common.${ProjectVisibility.Internal}`),
                        value: ProjectVisibility.Internal,
                    },
                    {
                        label: text(`common.${ProjectVisibility.Private}`),
                        value: ProjectVisibility.Private,
                    },
                ],
                rules: [Required],
            },
        },
    },
    {
        id: 'Step 2',
        name: 'Repository source',
        show: false,
        validated: false,
        skipValidation: false,
        status: StepStatus.UPCOMING,
        component: shallowRef(Step2),
        skipValidation: true,
        fields: {
            name: { label: 'repositoryName', type: 'text', rules: [Required] },
            host: { label: 'repositoryHost', type: 'text', rules: [Required] },
            id: {
                label: 'repositoryId',
                type: 'number',
                rules: [Required, IsNumber],
            },
            token: {
                label: 'repositoryToken',
                type: 'text',
                rules: [Required],
            },
            tokenExpiringDate: {
                label: 'repositoryTokenExpiringDate',
                type: 'date',
                rules: [FutureDate],
            },
        },
    },
    {
        id: 'Step 3',
        name: 'Teams',
        show: false,
        validated: false,
        skipValidation: true,
        status: StepStatus.UPCOMING,
        component: shallowRef(Step3),
        fields: {},
    },
    {
        id: 'Step 4',
        name: 'Review',
        show: false,
        validated: false,
        skipValidation: true,
        status: StepStatus.UPCOMING,
        component: shallowRef(Step4),
        fields: {},
    },
]);

const errors = ref([]);

const activeStep = computed((): Step | undefined =>
    steps.value.find((step) => step.show),
);

const scrollToErrors = () => {
    nextTick(() => {
        const errorElement = document.getElementById('form-errors');
        if (errorElement) {
            errorElement.scrollIntoView({
                behavior: 'smooth',
                block: 'center',
            });
        } else {
            console.warn('Error element not found');
        }
    });
};

const moveTo: Function<void> = (to: Step): void => {
    steps.value = steps.value.map((step: Step): void => ({
        ...step,
        show: step.id === to.id,
    }));
};

const nextStep: Function<void> = (): void => {
    let present: Step = steps.value.find(
        (step) => step.status === StepStatus.CURRENT,
    );
    if (!present.show) {
        return;
    }

    let validationResult;
    if (present.skipValidation) {
        validationResult = { valid: true, errors: [] };
    } else {
        validationResult = validate(form.value, present);
    }

    errors.value = validationResult.errors;
    if (errors.value.length > 0) {
        scrollToErrors();
        toast.error('There were errors with your submission.');
        return;
    }

    if (present) {
        present.status = StepStatus.COMPLETE;
        present.show = false;
    }

    const future: Step = steps.value.find(
        (step) => step.status === StepStatus.UPCOMING,
    );
    if (!future) {
        return;
    }
    future.status = StepStatus.CURRENT;
    future.show = true;
};

const isCompleted: ComputedGetter<boolean> = computed((): boolean =>
    steps.value.every((step) => step.status === StepStatus.COMPLETE),
);
const isLastStepCurrentStep: ComputedGetter<boolean> = computed(
    (): boolean =>
        steps.value[steps.value.length - 1].status === StepStatus.CURRENT,
);

const form = ref({
    organization: null,
    name: null,
    slug: null,
    status: ProjectStatus.Active,
    startDate: null,
    visibility: ProjectVisibility.Public,
    description: null,
    endDate: null,
    sprints: new Set<Sprint>(),
    tickets: new Set<Ticket>(),
    repositories: new Set<Repository>(),
    icon: null,
});

const submit: Function<void> = async (): void => {
    form.value.organization = useAuth().organization.value?.id;
    if (!form.value.organization) {
        toast.error('Organization not found');
        return;
    }

    const data = { ...form.value };

    const repositories = data.repositories;
    delete data.repositories;
    delete data.sprints;
    delete data.tickets;

    try {
        const created = await projectRepository.create(data);
        if (!created?.id) {
            toast.error('Error while creating project');
            return;
        }

        const createRelationPromises = [...repositories].map((repository) =>
            projectRepository
                .relation('repositories')
                .createRelation(created.id, repository),
        );

        await Promise.all(createRelationPromises);

        toast.success(
            'Project created successfully with all repositories linked!',
        );
        await useRouter().push({
            name: 'ProjectView',
            params: { projectId: created.id },
        });
    } catch (error) {
        console.error('Error during project creation:', error);
        toast.error('An error occurred while submitting the project.');
    }
};
</script>
