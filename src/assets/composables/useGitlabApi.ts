import { Repository } from '@/assets/types/models/Repository';
import { RepositoryType } from '@/assets/types/models/enums/RepositoryType';
import { Ref, ref } from 'vue';
import { GitlabIssue } from '@/assets/types/interfaces/repositories/Gitlab/GitlabIssue';
import { GitlabCommit } from '@/assets/types/interfaces/repositories/Gitlab/GitlabCommit';
import { GitlabMergeRequest } from '@/assets/types/interfaces/repositories/Gitlab/GitlabMergeRequest';
import { GitlabBranch } from '@/assets/types/interfaces/repositories/Gitlab/GitlabBranch';
import { GitlabRepository } from '@/assets/types/interfaces/repositories/Gitlab/GitlabRepository';

export function useGitlabApi(
    repositoryData: Repository = {
        project: null,
        remoteId: -1,
        type: RepositoryType.GITLAB,
        host: '',
        name: '',
        token: '',
        token_expiring_date: null,
        lastSynced: new Date(),
        created_at: new Date(),
        updated_at: new Date(),
    },
): {
    updateRepositoryData: (repository: Repository) => void;
    fetchRepositoryData: () => Promise<GitlabRepository | null>;
    fetchRepositoryCommits: (
        ref: string | null,
    ) => Promise<GitlabCommit[] | null>;
    fetchRepositoryBranches: () => Promise<GitlabBranch[] | null>;
    fetchRepositoryMergeRequests: () => Promise<GitlabBranch[] | null>;
    fetchRepositoryIssues: () => Promise<GitlabBranch[] | null>;
    fetchRepositoryReadMe: () => Promise<GitlabCommit | null>;
    fetchRepositorySummary: () => Promise<any | null>;
    ping: () => Promise<boolean>;
    isUsingDocker: () => Promise<{
        docker: boolean | null;
        k8s: boolean | null;
    }>;
} {
    const repository: Ref<Repository> = ref(repositoryData);
    const latestDockerCheck = ref(true);

    const get = async (
        url: string,
    ): Promise<GitlabRepository | GitlabCommit[] | GitlabBranch[] | null> => {
        try {
            const response = await fetch(url, {
                headers: {
                    Authorization: `Bearer ${repository.value.token}`,
                },
            });

            if (!response.ok) {
                console.error(
                    `[${url}] Failed to fetch GitLab data:`,
                    response.statusText,
                );
                return null;
            }

            return await response.json();
        } catch (error) {
            console.error(`[${url}] Error fetching GitLab data:`, error);
            return null;
        }
    };

    const updateRepositoryData = (repositoryData: Repository): void => {
        repository.value = repositoryData;
    };

    const fetchRepositoryData = async (): Promise<GitlabRepository | null> => {
        const apiUrl = `${repository.value?.host ?? 'https://gitlab.com'}/api/v4/projects/${repository.value.remoteId}?statistics=1&license=1&owner=1`;
        console.log('fetchRepositoryData', apiUrl);
        return await get(apiUrl);
    };

    const fetchRepositoryCommits = async (
        ref: string | null,
    ): Promise<GitlabCommit[] | null> => {
        let apiUrl = `${repository.value?.host ?? 'https://gitlab.com'}/api/v4/projects/${repository.value.remoteId}/repository/commits?with_stats=1`;
        if (ref) {
            apiUrl += `&ref_name=${ref}`;
        }
        console.log('fetchRepositoryCommits', apiUrl);
        return await get(apiUrl);
    };

    const fetchRepositoryBranches = async (): Promise<
        GitlabBranch[] | null
    > => {
        const apiUrl = `${repository.value?.host ?? 'https://gitlab.com'}/api/v4/projects/${repository.value.remoteId}/repository/branches`;
        console.log('fetchRepositoryBranches', apiUrl);
        return await get(apiUrl);
    };

    const fetchRepositoryMergeRequests = async (): Promise<
        GitlabMergeRequest[] | null
    > => {
        const apiUrl = `${repository.value?.host ?? 'https://gitlab.com'}/api/v4/projects/${repository.value.remoteId}/merge_requests?state=opened`;
        console.log('fetchRepositoryMergeRequests', apiUrl);
        return await get(apiUrl);
    };

    const fetchRepositoryIssues = async (): Promise<GitlabIssue[] | null> => {
        const apiUrl = `${repository.value?.host ?? 'https://gitlab.com'}/api/v4/projects/${repository.value.remoteId}/issues?state=opened`;
        console.log('fetchRepositoryIssues', apiUrl);
        return await get(apiUrl);
    };

    const fetchRepositoryStack = async (): Promise<Record<string, number>> => {
        const apiUrl = `${repository.value?.host ?? 'https://gitlab.com'}/api/v4/projects/${repository.value.remoteId}/languages`;
        console.log('fetchRepositoryStack', apiUrl);
        return await get(apiUrl);
    };

    const fetchRepositoryUsers = async (): Promise<
        Array<Record<string, number | string | unknown>>
    > => {
        const apiUrl = `${repository.value?.host ?? 'https://gitlab.com'}/api/v4/projects/${repository.value.remoteId}/users?show_seat_info=1`;
        console.log('fetchRepositoryStack', apiUrl);
        return await get(apiUrl);
    };

    const fetchRepositoryReadMe = async (): Promise<GitlabCommit | null> => {
        const apiUrl = `${repository.value?.host ?? 'https://gitlab.com'}/api/v4/projects/${repository.value.remoteId}/repository/files/README.md?ref=main`;
        console.log('fetchRepositoryReadMe', apiUrl);
        return await get(apiUrl);
    };

    const ping = async (): Promise<boolean> => {
        const apiUrl = `${repository.value?.host ?? 'https://gitlab.com'}/api/v4/projects/${repository.value.remoteId}`;
        const returnValue = await get(apiUrl);
        return !!returnValue?.id;
    };

    const isUsingDocker = async (): Promise<{
        docker: boolean | null;
        k8s: boolean | null;
    }> => {
        if (!latestDockerCheck.value) {
            return {
                docker: false,
                k8s: false,
            };
        }

        const baseUrl = `${repository.value?.host ?? 'https://gitlab.com'}/api/v4/projects/${repository.value.remoteId}/repository/files`;

        const [
            dockerfile,
            dockerComposeYaml,
            dockerComposeYml,
            k8sPodYaml,
            k8sPodYml,
            k8sManifestYaml,
            k8sManifestYml,
        ] = await Promise.all([
            get(baseUrl + '/Dockerfile?ref=main'),
            get(baseUrl + '/docker-compose.yaml?ref=main'),
            get(baseUrl + '/docker-compose.yml?ref=main'),
            get(baseUrl + '/pod.yaml?ref=main'),
            get(baseUrl + '/pod.yml?ref=main'),
            get(baseUrl + '/deployment.yaml?ref=main'),
            get(baseUrl + '/deployment.yml?ref=main'),
        ]);

        const result = {
            docker: !!dockerfile || !!dockerComposeYaml || !!dockerComposeYml,
            k8s:
                !!k8sPodYaml ||
                !!k8sPodYml ||
                !!k8sManifestYaml ||
                !!k8sManifestYml,
        };

        if (Object.values(result).every((value) => !value)) {
            latestDockerCheck.value = false;
        }

        return result;
    };

    const fetchRepositorySummary = async (): Promise<any | null> => {
        const summary: {
            users: any | null;
            details: GitlabRepository | null;
            stack: Record<string, number> | null;
            docker: { docker: boolean | null; k8s: boolean | null } | null;
            branches: GitlabBranch[] | null;
            mrs: GitlabMergeRequest[] | null;
            issues: GitlabIssue[] | null;
        } = {
            users: null,
            details: null,
            stack: null,
            docker: null,
            branches: null,
            issues: null,
            mrs: null,
        };

        const [details, users, stack, docker, branches, issues, mrs] =
            await Promise.all([
                fetchRepositoryData(),
                fetchRepositoryUsers(),
                fetchRepositoryStack(),
                isUsingDocker(),
                fetchRepositoryBranches(),
                fetchRepositoryIssues(),
                fetchRepositoryMergeRequests(),
            ]);

        summary.details = details;
        summary.users = users;
        summary.stack = stack;
        summary.docker = docker;
        summary.branches = branches;
        summary.issues = issues;
        summary.mrs = mrs;
        console.log('fetchRepositorySummary', summary);

        return summary;
    };

    return {
        fetchRepositoryData,
        updateRepositoryData,
        fetchRepositoryCommits,
        fetchRepositoryBranches,
        fetchRepositoryMergeRequests,
        fetchRepositoryIssues,
        fetchRepositorySummary,
        fetchRepositoryReadMe,
        ping,
        isUsingDocker,
    };
}
