export default function useApi(): {
    get: <T>(url: string) => Promise<T>;
    post: <T>(url: string, data: unknown) => Promise<T>;
    patch: <T>(url: string, data: unknown) => Promise<T>;
    put: <T>(url: string, data: unknown) => Promise<T>;
    delete: <T>(url: string) => Promise<T>;
} {
    const apiHost = import.meta.env.VITE_API_HOST || '/api';

    const request = async <T>(
        url: string,
        options: RequestInit,
    ): Promise<T> => {
        const fullUrl = new URL(url, apiHost).toString();

        const response = await fetch(fullUrl, {
            ...options,
            headers: {
                'Content-Type': 'application/json',
                ...(options.headers ?? {}),
            },
        });

        if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }

        return (await response.json()) as T;
    };

    return {
        get: async <T>(url: string) => await request<T>(url, { method: 'GET' }),
        post: async <T>(url: string, data: unknown) =>
            await request<T>(url, {
                method: 'POST',
                body: JSON.stringify(data),
            }),
        patch: async <T>(url: string, data: unknown) =>
            await request<T>(url, {
                method: 'PATCH',
                body: JSON.stringify(data),
            }),
        put: async <T>(url: string, data: unknown) =>
            await request<T>(url, {
                method: 'PUT',
                body: JSON.stringify(data),
            }),
        delete: async <T>(url: string) =>
            await request<T>(url, { method: 'DELETE' }),
    };
}
