import { InputDescriptor } from '@/assets/types/components/InputDescriptor';
import { Step } from '@/assets/types/components/Step';
import { InputRule } from '@/assets/types/components/inputRules/InputRule';

export function useForm(): {
    validate: (formData, step: Step) => { valid: boolean; errors: string[] };
    validateForm: (
        formData,
        fields: Record<string, InputDescriptor>
    ) => { valid: boolean; errors: string[] };
} {
    const validateForm = (
        form,
        fields: Record<string, InputDescriptor>
    ): { valid: boolean; errors: string[] } => {
        const errors: string[] = [];
        for (const key in fields) {
            const rules: InputRule[] | undefined = fields[key].rules;
            if (rules?.length) {
                rules.forEach((rule) => {
                    const isRuleRespected = rule.apply(form[key]);
                    if (isRuleRespected) {
                        return isRuleRespected;
                    }

                    errors.push(fields[key].label);
                });
            }
        }

        return { valid: errors.length === 0, errors };
    }

    const validate = (
        form,
        step: Step,
    ): { valid: boolean; errors: string[] } => {
        return validateForm(form, step.fields)
    };

    return {
        validate: validate,
        validateForm: validateForm
    };
}
