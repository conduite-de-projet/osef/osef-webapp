export function useSizeFormatter(): {
    parseSize: (size: string) => number;
    formatSize: (bytes: number) => string;
} {
    const parseSize = (size: string): number => {
        const units: Record<string, number> = {
            o: 1,
            ko: 1024,
            mo: 1024 ** 2,
            go: 1024 ** 3,
            to: 1024 ** 4,
        };

        const match = size.toLowerCase().match(/^([0-9]+)(o|ko|mo|go|to)$/);
        if (!match) {
            throw new Error('Invalid size format');
        }

        const value = parseInt(match[1], 10);
        const unit = match[2];
        return value * (units[unit] || 1);
    };

    const formatSize = (bytes: number): string => {
        if (bytes < 1024) return `${bytes}o`;
        if (bytes < 1024 ** 2) {
            return `${(bytes / 1024).toFixed(bytes % 1024 === 0 ? 0 : 2)}Ko`;
        }
        if (bytes < 1024 ** 3) {
            return `${(bytes / 1024 ** 2).toFixed(bytes % 1024 ** 2 === 0 ? 0 : 2)}Mo`;
        }
        if (bytes < 1024 ** 4) {
            return `${(bytes / 1024 ** 3).toFixed(bytes % 1024 ** 3 === 0 ? 0 : 2)}Go`;
        }
        return `${(bytes / 1024 ** 4).toFixed(bytes % 1024 ** 4 === 0 ? 0 : 2)}To`;
    };

    return {
        parseSize,
        formatSize,
    };
}
