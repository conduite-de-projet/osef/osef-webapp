import { Repository } from '@/assets/types/models/Repository';
import GitLabIcon from '@/assets/svg/GitLabIcon.vue';
import { RepositoryType } from '@/assets/types/models/enums/RepositoryType';
import GitHubIcon from '@/assets/svg/GitHubIcon.vue';
import GitLabSelfHostedIcon from '@/assets/svg/GitLabSelfHostedIcon.vue';
import { RepositoryDefinition } from '@/assets/types/components/RepositoryDefinition';
import * as HeroIcons from '@heroicons/vue/24/solid';
import { computed, ComputedRef } from 'vue';
import {
    GitlabCommit,
    GitlabRepository,
    useGitlabApi,
} from '@/assets/composables/useGitlabApi';
import {
    GitHubCommit,
    GithubRepository,
    useGithubApi,
} from '@/assets/composables/useGithubApi';

export default function useRemoteRepository(): {
    isSet: (repository: Repository) => boolean;
    definitions: RepositoryDefinition[];
    icons: object[];
    colors: string[];
    api: ComputedRef<{
        fetchRepositoryData: (
            remoteId: number,
            token: string,
            baseUrl?: string,
        ) => Promise<any>;
        ping: () => boolean;
    } | null>;
} {
    const isSet = (repository: Repository): boolean => {
        return (
            repository.type &&
            repository.remoteId &&
            repository.host &&
            repository.name &&
            repository.token &&
            repository.lastSynced
        );
    };

    const definitions: RepositoryDefinition[] = [
        {
            title: 'Gitlab',
            description: 'Search a gitlab project',
            icon: GitLabIcon,
            type: RepositoryType.GITLAB,
            size: 'size-12',
            background: 'bg-accent-pink',
        },
        {
            title: 'Github',
            description: 'Search a github project',
            icon: GitHubIcon,
            type: RepositoryType.GITHUB,
            size: 'size-8',
            background: 'bg-accent-blue',
        },
        {
            title: 'Gitlab - Self managed',
            description:
                'Search a gitlab project on your self-managed gitlab instance',
            icon: GitLabSelfHostedIcon,
            type: RepositoryType.GITLAB_HOSTED,
            size: 'size-12',
            background: 'bg-accent-green',
        },
    ];

    const api: ComputedRef<{
        updateRepositoryData: (repository: Repository) => void;
        fetchRepositoryData: () => Promise<
            GitlabRepository | GithubRepository | null
        >;
        fetchRepositoryCommits: () => Promise<
            GitlabCommit[] | GitHubCommit[] | null
        >;
    } | null> = computed(() => (repository: Repository) => {
        if (
            [RepositoryType.GITLAB, RepositoryType.GITLAB_HOSTED].includes(
                repository.type,
            )
        ) {
            return useGitlabApi(repository);
        }
        if (props.repository.type === RepositoryType.GITHUB) {
            return useGithubApi(repository);
        }
        return null;
    });

    const icons = Object.freeze([
        { name: 'CodeBracket', icon: HeroIcons.CodeBracketIcon },
        { name: 'CircleStack', icon: HeroIcons.CircleStackIcon },
        { name: 'ServerStack', icon: HeroIcons.ServerStackIcon },
        { name: 'Cloud', icon: HeroIcons.CloudIcon },
        { name: 'GlobeAsiaAustralia', icon: HeroIcons.GlobeAsiaAustraliaIcon },
        { name: 'QrCode', icon: HeroIcons.QrCodeIcon },
        { name: 'Cog', icon: HeroIcons.CogIcon },
        { name: 'LightBulb', icon: HeroIcons.LightBulbIcon },
        { name: 'Users', icon: HeroIcons.UsersIcon },
        { name: 'ChartBar', icon: HeroIcons.ChartBarIcon },
        { name: 'ShieldCheck', icon: HeroIcons.ShieldCheckIcon },
        { name: 'CommandLine', icon: HeroIcons.CommandLineIcon },
        { name: 'Tag', icon: HeroIcons.TagIcon },
        { name: 'Briefcase', icon: HeroIcons.BriefcaseIcon },
        { name: 'Folder', icon: HeroIcons.FolderIcon },
        { name: 'Wrench', icon: HeroIcons.WrenchIcon },
        { name: 'DevicePhoneMobile', icon: HeroIcons.DevicePhoneMobileIcon },
        { name: 'FingerPrint', icon: HeroIcons.FingerPrintIcon },
        { name: 'CubeTransparent', icon: HeroIcons.CubeTransparentIcon },
        { name: 'Cube', icon: HeroIcons.CubeIcon },
    ]);

    const colors = Object.freeze([
        '#F87171',
        '#60A5FA',
        '#34D399',
        '#FBBF24',
        '#A78BFA',
        '#E879F9',
        '#93C5FD',
        '#FCD34D',
        '#86EFAC',
        '#22D3EE',
    ]);

    const languageColors: Readonly<Record<string, string>> = Object.freeze({
        Dockerfile: '#1D63ED',
        TypeScript: '#3178C6',
        Java: '#ED8B00',
        Shell: '#89e051',
        Ruby: '#701516',
        JavaScript: '#f1e05a',
        HTML: '#e34c26',
        CSS: '#1F75FE',
        Vue: '#42b883',
        Default: '#cccccc',
    });

    return {
        isSet,
        definitions,
        icons,
        colors,
        languageColors,
        api,
    };
}
