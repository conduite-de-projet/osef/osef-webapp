import useApi from './useApi';
import Repository from '../types/interfaces/repository';
import { Filters } from '../types/filters';
import { ref } from 'vue';

export default function useRepository<T>(endpoint: string): Repository<T> {
    const api = useApi();
    const loadedRelation = ref('');

    const getAll = (filters?: Filters): Promise<T[]> => {
        const query = filters
            ? '?' +
              new URLSearchParams(filters as Record<string, string>).toString()
            : '';
        return api.get<T[]>(`/${endpoint}${query}`);
    };

    const filterBy = (filters: Filters): (() => Promise<T[]>) => {
        return () => getAll(filters);
    };

    const relation = (related: string): Repository<T> => {
        loadedRelation.value = related;
        return {
            getAll,
            getById,
            create,
            update,
            delete: _delete,
            filterBy,
            relation,
            createRelation,
            activeRelation: loadedRelation.value,
        };
    };

    const getById = (id: number | string): ((url: string) => Promise<T>) => {
        let ep = `/${endpoint}/${id}`;
        if (loadedRelation.value.length > 0) {
            ep += `/${loadedRelation.value}`;
            loadedRelation.value = null;
        }

        return api.get<T>(ep);
    };

    const create = (data: Partial<T>): Promise<T> =>
        api.post<T>(`/${endpoint}`, data);

    const update = (id: string | number, data: Partial<T>): Promise<T> =>
        api.put<T>(`/${endpoint}/${id}`, data);

    const _delete = (id: string | number): Promise<void> =>
        api.delete<void>(`/${endpoint}/${id}`);

    const createRelation = (
        source: number | string,
        data: Partial<any>,
    ): ((url: string, data: Partial<T>) => Promise<T>) => {
        if (!loadedRelation.value) {
            throw new Error('Missing relation');
        }

        const ep = `/${endpoint}/${source}/${loadedRelation.value}`;
        loadedRelation.value = null;

        return api.post<T>(ep, data);
    };

    return {
        getAll,
        getById,
        create,
        createRelation,
        update,
        delete: _delete,
        filterBy,
        relation,
        activeRelation: loadedRelation.value,
    };
}
