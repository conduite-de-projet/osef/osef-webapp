import { useText } from '@/assets/composables/useText';

export default function useDates(): {
    formatTimestamp: (timestamp: number, formatString: string) => string;
    now: (formatString: string) => string;
    toDatetime: (dateString: string) => Date | null;
    toRelativeTime: (date: Date) => string;
    daysSince: (date: Date) => number;
    daysUntil: (date: Date) => number;
} {
    const { text } = useText();

    const formatTimestamp: (
        timestamp: number,
        formatString: string,
    ) => string = (
        timestamp: number,
        formatString = 'DD/MM/YYYY HH:mm:ss',
    ): string => {
        const date = new Date(timestamp);

        const map: Record<string, string | number> = {
            YYYY: date.getFullYear(),
            MM: String(date.getMonth() + 1).padStart(2, '0'),
            DD: String(date.getDate()).padStart(2, '0'),
            HH: String(date.getHours()).padStart(2, '0'),
            mm: String(date.getMinutes()).padStart(2, '0'),
            ss: String(date.getSeconds()).padStart(2, '0'),
        };

        return formatString.replace(
            /(YYYY|MM|DD|HH|mm|ss)/g,
            (match) => map[match] as string,
        );
    };

    const now: (formatString: string) => string = (
        formatString = 'DD/MM/YYYY HH:mm:ss',
    ): string => {
        return formatTimestamp(Date.now(), formatString);
    };

    const toDatetime: (dateString: string) => string = (
        dateString: string,
    ): Date | null => {
        const parsedDate = new Date(dateString);
        if (isNaN(parsedDate.getTime())) {
            throw new Error('Invalid date string');
        }
        return parsedDate;
    };

    const toRelativeTime: (date: Date) => string = (
        date: string | Date,
    ): string => {
        if (!date) {
            return '';
        }

        if (typeof date === 'string') {
            const parsedDate = new Date(date);
            if (isNaN(parsedDate.getTime())) {
                throw new Error('Invalid date string');
            }
            date = parsedDate;
        }

        const units = [
            {
                name: text('common.times.short.year'),
                seconds: 365 * 24 * 60 * 60,
            },
            {
                name: text('common.times.short.month'),
                seconds: 30 * 24 * 60 * 60,
            },
            { name: text('common.times.short.day'), seconds: 24 * 60 * 60 },
            { name: text('common.times.short.hour'), seconds: 60 * 60 },
            { name: text('common.times.short.minute'), seconds: 60 },
            { name: text('common.times.short.second'), seconds: 1 },
        ];

        const diffInSeconds = Math.floor(
            (new Date().getTime() - date.getTime()) / 1000,
        );

        for (const unit of units) {
            const value = Math.floor(diffInSeconds / unit.seconds);
            if (value > 0) {
                const relative = `${value}${unit.name}${value > 1 && [text('common.times.short.year'), text('common.times.short.month')].includes(unit.name) ? 's' : ''}`;
                return text('common.times.relative', relative);
            }
        }

        return text('common.times.now');
    };

    function daysSince(date: Date): number {
        const today = new Date();
        const givenDate = date;

        if (givenDate > today) {
            return 0;
        }

        const diffInMilliseconds = today.getTime() - givenDate.getTime();

        return Math.ceil(diffInMilliseconds / (1000 * 60 * 60 * 24));
    }

    function daysUntil(date: Date): number {
        const today = new Date();
        const givenDate = new Date(date);

        const diffInMilliseconds = givenDate.getTime() - today.getTime();

        return Math.floor(diffInMilliseconds / (1000 * 60 * 60 * 24));
    }

    return {
        formatTimestamp,
        now,
        toDatetime,
        toRelativeTime,
        daysSince,
        daysUntil,
    };
}
