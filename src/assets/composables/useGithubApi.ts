import { GitlabRepository } from '@/assets/composables/useGitlabApi';
import { Repository } from '@/assets/types/models/Repository';
import { RepositoryType } from '@/assets/types/models/enums/RepositoryType';
import { ref } from 'vue';

export interface GithubRepository {
    id: number;
    name: string;
    full_name: string;
    html_url: string;

    [key: string]: unknown;
}

export interface GitHubCommit {
    id: string;
    short_id: string;
    title: string;
    author_name: string;
    author_email: string;
    authored_date: string;
    committer_name: string;
    committer_email: string;
    committed_date: string;
    created_at: string;
    message: string;
    parent_ids: string[];
    web_url: string;
    trailers: object;
    extended_trailers: object;
}

export function useGithubApi(
    repositoryData: Repository = {
        project: null,
        remoteId: -1,
        type: RepositoryType.GITLAB,
        host: '',
        name: '',
        token: '',
        token_expiring_date: null,
        lastSynced: new Date(),
        created_at: new Date(),
        updated_at: new Date(),
    },
): {
    updateRepositoryData: (repository: Repository) => void;
    fetchRepositoryData: (
        remoteId: number,
        token: string,
        baseUrl?: string,
    ) => Promise<GitlabRepository | null>;
} {
    const repository = ref(repositoryData);

    const updateRepositoryData = (repositoryData: Repository) =>
        (repository.value = repositoryData);

    const fetchRepositoryData = async (): Promise<GithubRepository | null> => {
        const apiUrl = `https://api.github.com/repos/${repository.value.remoteId}`;
        try {
            const response = await fetch(apiUrl, {
                headers: {
                    Authorization: `Bearer ${repository.value.token}`,
                },
            });

            if (!response.ok) {
                console.error(
                    'Failed to fetch GitHub repository data:',
                    response.statusText,
                );
                return null;
            }

            return await response.json();
        } catch (error) {
            console.error('Error fetching GitHub repository data:', error);
            return null;
        }
    };

    const fetchRepositoryCommits = async (): Promise<GitHubCommit | null> => {
        // TODO
        throw new Error('Not implemented');
    };

    return {
        fetchRepositoryData,
        updateRepositoryData,
        fetchRepositoryCommits,
    };
}
