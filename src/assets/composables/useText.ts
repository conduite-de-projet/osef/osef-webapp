import { InputDescriptor } from '@/assets/types/components/InputDescriptor';
import { getCurrentInstance } from 'vue';

export function useText(): {
    text: (path: string) => string;
} {
    const instance = getCurrentInstance();

    const text: (path: string) => string =
        instance?.appContext.config.globalProperties.$text;

    return {
        text,
    };
}
