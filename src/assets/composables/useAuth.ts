import { ref, computed, ComputedRef } from 'vue';
import useApi from '@/assets/composables/useApi';
import { User } from '@/assets/types/models/User';
import { Organization } from '../types/models/Organization';
import { toast } from 'vue3-toastify';

const currentUser = ref<User | null>(null);

export default function useAuth(): {
    login: (email: string, password: string) => Promise<void>;
    logout: () => Promise<void>;
    registerOrganization: (organization: Organization) => void
    user: ComputedRef<User | null>;
    organization: ComputedRef<Organization | null>;
    isAuthenticated: ComputedRef<boolean>;
} {
    const api = useApi();

    const login = async (email: string, password: string): Promise<void> => {
        try {
            const response: User = await api.post<User>('/users/login', {
                email,
                passwordHash: password,
            });
            currentUser.value = JSON.stringify(response);
            localStorage.setItem('user', currentUser.value);
        } catch (error) {
            toast.error('Login failed, reason: ' + JSON.stringify(error));
            throw new Error('Login failed, reason: ' + JSON.stringify(error));
        }
    };

    const logout = async (): Promise<void> => {
        try {
            //await api.post('/auth/logout');
            currentUser.value = null;
            localStorage.removeItem('user');
            localStorage.removeItem('organization');
        } catch (error) {
            toast.error('Logout failed, reason: ' + JSON.stringify(error));
            throw new Error('Logout failed, reason: ' + JSON.stringify(error));
        }
    };

    const registerOrganization = ((organization: Organization) => {
        localStorage.setItem('organization', JSON.stringify(organization));
    })

    const organization = computed<Organization | null>(() => {
        const organizationString = localStorage.getItem('organization');
        if (!organizationString) {
            return null
        }
        return JSON.parse(organizationString) as Organization;
    });

    const user = computed<User>(() => {
        const userString = localStorage.getItem('user');
        if (!userString) {
            throw new Error('Invalid user');
        }
        return JSON.parse(userString) as User;
    });

    const isAuthenticated = computed(() => !!currentUser.value);

    return { login, logout, registerOrganization, user, organization, isAuthenticated };
}
