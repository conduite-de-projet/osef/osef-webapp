import { Middleware } from '../types/middleware';

function* middlewareGenerator(
    middlewares: Middleware[],
): Generator<Middleware, void, unknown> {
    for (const middleware of middlewares) {
        yield middleware;
    }
}

const middlewarePipe = (middlewares: Middleware[]): Middleware => {
    return (to, from, next) => {
        const generator = middlewareGenerator(middlewares);
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const executeNextMiddleware = (nextArg?: any): void => {
            if (nextArg) {
                next(nextArg);
                return;
            }

            const { value: currentMiddleware, done } = generator.next();
            if (done) {
                next();
            } else {
                currentMiddleware(to, from, executeNextMiddleware);
            }
        };

        executeNextMiddleware();
    };
};

export default middlewarePipe;
