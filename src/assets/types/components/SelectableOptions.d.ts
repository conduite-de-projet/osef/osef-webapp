export type SelectableOptions = {
    label: string;
    value: number | string | boolean;
};
