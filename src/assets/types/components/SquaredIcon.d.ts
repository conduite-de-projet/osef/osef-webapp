export type SquaredIcon = {
    title: string;
    description: string;
    icon: any;
    size: string;
    background: string;
};
