import { InputRule } from '@/assets/types/components/inputRules/InputRule';

export class IsNumber extends InputRule {
    static apply(value): boolean {
        return !Number.isNaN(value);
    }
}
