import { InputRule } from '@/assets/types/components/inputRules/InputRule';

export class PasswordRegex extends InputRule {
    static apply(value: string): boolean {
        const passwordRegex =
            /^(?=.*[&”'()![\]{},;:\-_#@])(?=.*[A-Z])(?=.*[a-z])(?=.*\d).{10,}$/;
        return passwordRegex.test(value);
    }
}
