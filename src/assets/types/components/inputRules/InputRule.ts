export abstract class InputRule {
    static apply(value): boolean;
}
