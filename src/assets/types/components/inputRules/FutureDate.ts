import { InputRule } from '@/assets/types/components/inputRules/InputRule';

export class FutureDate extends InputRule {
    static apply(value?: Date): boolean {
        if (value === undefined || value === null) {
            return false;
        }
        const now = new Date();
        return value.getTime() > now.getTime();
    }
}
