import { Required } from './Required';

// Enforce non null string (blank text)
export class TextRequired extends Required {
    static apply(value: string): boolean {
        return super.apply(value) && value.replace(/\s/g, '').length !== 0;
    }
}
