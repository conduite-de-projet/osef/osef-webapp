import { InputRule } from '@/assets/types/components/inputRules/InputRule';

export class MinLength extends InputRule {
    private readonly min: number = 1;

    constructor(min: number) {
        super();
        this.min = Math.abs(min);
        return this;
    }

    static apply(value: Iterable<any>): boolean {
        return [...value].length >= this.min;
    }
}
