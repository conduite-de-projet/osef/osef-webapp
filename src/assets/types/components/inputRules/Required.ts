import { InputRule } from '@/assets/types/components/inputRules/InputRule';

export class Required extends InputRule {
    static apply(value): boolean {
        return value !== undefined && value !== null;
    }
}
