import { RepositoryType } from '@/assets/types/models/enums/RepositoryType';
import { SquaredIcon } from '@/assets/types/components/SquaredIcon';

export type RepositoryDefinition = SquaredIcon & {
    type: RepositoryType;
};
