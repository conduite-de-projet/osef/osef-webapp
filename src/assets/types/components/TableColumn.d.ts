export class TableColumn<T> {
    key: keyof T;
    label: string;
    isLink?: boolean;
}
