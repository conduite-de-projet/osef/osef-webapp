import { StepStatus } from '@/assets/types/helpers/enums/StepStatus';
import { Component } from 'vue';
import { InputDescriptor } from '@/assets/types/components/InputDescriptor';

export class Step {
    id: string;
    name: string;
    show: boolean;
    status: StepStatus;
    component: Component;
    fields: Record<string, InputDescriptor>;
}
