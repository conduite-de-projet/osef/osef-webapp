export type Filters = Record<string, string | number | boolean | null>;
