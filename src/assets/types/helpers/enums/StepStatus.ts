export enum StepStatus {
    CURRENT = 'CURRENT',
    COMPLETE = 'COMPLETE',
    UPCOMING = 'UPCOMING',
}
