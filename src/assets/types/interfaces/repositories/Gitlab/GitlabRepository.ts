export interface GitlabRepository {
    id: number;
    name: string;
    web_url: string;

    [key: string]: unknown;
}
