export interface GitlabIssue {
    id: number;
    name: string;
    web_url: string;

    [key: string]: unknown;
}
