import { Repository } from '@/assets/types/models/Repository';
import { ProjectStatus } from '@/assets/types/models/enums/ProjectStatus';
import { ProjectVisibility } from '@/assets/types/models/enums/ProjectVisibility';
import { Sprint } from '@/assets/types/models/Sprint';
import { Ticket } from '@/assets/types/models/Ticket';

export interface ProjectForm {
    organization: string | null;
    name: string | null;
    slug: string | null;
    status: ProjectStatus;
    startDate: Date | null;
    visibility: ProjectVisibility;
    description: string | null;
    endDate: Date | null;
    sprints: Set<Sprint>;
    tickets: Set<Ticket>;
    repositories: Set<Repository>;
    icon: string | null;
}
