import GitLabIcon from '@/assets/svg/GitLabIcon.vue';
import GitHubIcon from '@/assets/svg/GitHubIcon.vue';
import GitLabSelfHostedIcon from '@/assets/svg/GitLabSelfHostedIcon.vue';
import { RepositoryType } from '@/assets/types/models/enums/RepositoryType';

export interface RepositoryItem {
    title: string;
    description: string;
    icon: typeof GitLabIcon | typeof GitHubIcon | typeof GitLabSelfHostedIcon;
    type: RepositoryType;
    size: string;
    background: string;
}
