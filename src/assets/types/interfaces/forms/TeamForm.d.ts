import { User } from '@/assets/types/models/user.ts';

export interface TeamForm {
    name: string;
    description: string | null;
    members: string[];
}
