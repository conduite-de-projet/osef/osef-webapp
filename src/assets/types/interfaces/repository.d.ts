export default interface Repository<T> {
    getAll: (
        filters?: Record<string, string | number | boolean | null>,
    ) => Promise<T[]>;
    getById: (id: number | string) => Promise<T>;
    create: (data: Partial<T>) => Promise<T>;
    update: (id: number | string, data: Partial<T>) => Promise<T>;
    delete: (id: number | string) => Promise<void>;
    filterBy: (
        filters: Record<string, string | number | boolean | null>,
    ) => () => Promise<T[]>;
    relation: (related: string) => Repository<T>;
    createRelation: (source: number | string, data: Partial<T>) => Promise<T>;
    activeRelation: string | null;
}
