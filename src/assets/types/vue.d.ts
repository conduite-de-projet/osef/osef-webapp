import en from '../assets/content/texts/en.json';
import fr from '../assets/content/texts/fr.json';
import { Locale } from './locale';

declare module '@vue/runtime-core' {
    interface ComponentCustomProperties {
        $texts: Record<Locale, typeof en | typeof fr>;
        $locale: Locale;
        $availableLocales: Locale[];
        $text: (path: string) => string;
        $switchLocale: (newLocale: Locale) => void;
        $textExists: (path: string) => boolean;
        $colors: {
            gradients: {
                from: string;
                to: string;
            };
        };
    }
}
