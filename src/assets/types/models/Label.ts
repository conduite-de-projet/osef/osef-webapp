import { TicketLabel } from '@/assets/types/models/TicketLabel';
import { BaseModel } from '@/assets/types/models/BaseModel';

export class Label extends BaseModel {
    name: string;
    color: string;
    created_at: Date;
    updated_at: Date;
    tickets: Set<TicketLabel>;
}
