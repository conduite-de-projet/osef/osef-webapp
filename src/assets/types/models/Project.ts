import { Repository } from '@/assets/types/models/Repository';
import { Organization } from '@/assets/types/models/Organization';
import { Sprint } from '@/assets/types/models/Sprint';
import { Ticket } from '@/assets/types/models/Ticket';
import { ProjectStatus } from '@/assets/types/models/enums/ProjectStatus';
import { ProjectVisibility } from '@/assets/types/models/enums/ProjectVisibility';
import { BaseModel } from '@/assets/types/models/BaseModel';

export class Project extends BaseModel {
    private _organization: Organization;
    private _name: string;
    private _description?: string | null;
    private _status: ProjectStatus;
    private _start_date: Date;
    private _end_date?: Date | null;
    private _visibility: ProjectVisibility;
    private _created_at: Date;
    private _updated_at: Date;
    private _sprints: Set<Sprint>;
    private _tickets: Set<Ticket>;
    private _repositories: Set<Repository>;
    private _icon?: string;

    constructor(
        organization: Organization,
        name: string,
        status: ProjectStatus,
        start_date: Date,
        visibility: ProjectVisibility,
        created_at: Date,
        updated_at: Date,
        description: string | null = null,
        end_date: Date | null = null,
        sprints: Set<Sprint> = new Set<Sprint>(),
        tickets: Set<Ticket> = new Set<Ticket>(),
        repositories: Set<Repository> = new Set<Repository>(),
        icon: string | null = null,
    ) {
        super();
        this._organization = organization;
        this._name = name;
        this._description = description;
        this._status = status;
        this._start_date = start_date;
        this._end_date = end_date;
        this._visibility = visibility;
        this._created_at = created_at;
        this._updated_at = updated_at;
        this._sprints = sprints;
        this._tickets = tickets;
        this._repositories = repositories;
        this._icon = icon;
    }

    get organization(): Organization {
        return this._organization;
    }

    set organization(value: Organization) {
        this._organization = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get description(): string | null {
        return this._description ?? null;
    }

    set description(value: string | null) {
        this._description = value;
    }

    get status(): ProjectStatus {
        return this._status;
    }

    set status(value: ProjectStatus) {
        this._status = value;
    }

    get start_date(): Date {
        return this._start_date;
    }

    set start_date(value: Date) {
        this._start_date = value;
    }

    get end_date(): Date | null {
        return this._end_date ?? null;
    }

    set end_date(value: Date | null) {
        this._end_date = value;
    }

    get visibility(): ProjectVisibility {
        return this._visibility;
    }

    set visibility(value: ProjectVisibility) {
        this._visibility = value;
    }

    get created_at(): Date {
        return this._created_at;
    }

    set created_at(value: Date) {
        this._created_at = value;
    }

    get updated_at(): Date {
        return this._updated_at;
    }

    set updated_at(value: Date) {
        this._updated_at = value;
    }

    get sprints(): Set<Sprint> {
        return this._sprints;
    }

    set sprints(value: Set<Sprint>) {
        this._sprints = value ?? new Set<Sprint>();
    }

    get tickets(): Set<Ticket> {
        return this._tickets;
    }

    set tickets(value: Set<Ticket>) {
        this._tickets = value ?? new Set<Ticket>();
    }

    get repositories(): Set<Repository> {
        return this._repositories;
    }

    set repositories(value: Set<Repository>) {
        this._repositories = value ?? new Set<Repository>();
    }

    get icon(): string {
        return this._icon;
    }

    set icon(value: string) {
        this._icon = value;
    }
}
