import { Project } from '@/assets/types/models/Project';
import { BaseModel } from '@/assets/types/models/BaseModel';
import { RepositoryType } from '@/assets/types/models/enums/RepositoryType';

export class Repository extends BaseModel {
    project: Project | null;
    remoteId: number;
    type: RepositoryType;
    host: string;
    name: string;
    token?: string;
    token_expiring_date?: Date;
    lastSynced: Date;
    created_at: Date;
    updated_at: Date;
}
