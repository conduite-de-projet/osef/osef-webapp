import { Team } from './Team';
import { Ticket } from '@/assets/types/models/Ticket';
import { OrganizationUserRole } from '@/assets/types/models/OrganizationUserRole';
import { Country } from '@/assets/types/models/enums/Country';
import { UserStatus } from '@/assets/types/models/enums/UserStatus';
import { BaseModel } from '@/assets/types/models/BaseModel';
import { UserColors } from '@/assets/types/enums/UserColors'

export class User extends BaseModel {
    teams: Set<Team>;
    roles: Set<OrganizationUserRole>;
    tickets: Set<Ticket>;
    comments: Set<Comment>;
    firstName: string;
    lastName: string;
    email: string;
    passwordHash?: string;
    status: UserStatus;
    last_login?: Date;
    created_at: Date;
    updated_at: Date;
    website?: string;
    bio?: string;
    profileColor: UserColors;
    country: Country;
    address: string;
    city: string;
    state: string;
    zipCode: string;
    notifications: string[];
    contact_method: string;
}
