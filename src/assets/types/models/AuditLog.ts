import { Ticket } from '@/assets/types/models/Ticket';
import { User } from '@/assets/types/models/User';
import { BaseModel } from '@/assets/types/models/BaseModel';

export class AuditLog extends BaseModel {
    user: User | null;
    ticket?: Ticket | null;
    action?: string;
    description?: string;
    ip_address?: string;
    created_at: Date;
}
