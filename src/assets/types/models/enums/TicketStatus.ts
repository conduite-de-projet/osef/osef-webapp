import { BaseTicketStatus } from '@/assets/types/models/enums/BaseTicketStatus';

export type TicketStatus = BaseTicketStatus | string;
