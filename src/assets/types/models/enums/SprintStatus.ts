export enum SprintStatus {
    Planned = 'planned',
    InProgress = 'in_progress',
    Completed = 'completed',
}
