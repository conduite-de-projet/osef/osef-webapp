export enum UserColors {
    red = 'bg-red-400',
    blue = 'bg-blue-400',
    yellow = 'bg-yellow-400',
    purple = 'bg-purple-400',
    orange = 'bg-orange-400',
}