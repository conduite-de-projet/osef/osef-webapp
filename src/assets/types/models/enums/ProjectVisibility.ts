export enum ProjectVisibility {
    Private = 'private',
    Public = 'public',
    Internal = 'internal',
}
