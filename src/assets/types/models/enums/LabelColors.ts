export enum LabelColors {
    LightPink = 'bg-[#FFB6C1]',
    LightBlue = 'bg-[#ADD8E6]',
    LightGreen = 'bg-[#90EE90]',
    Lavender = 'bg-[#E6E6FA]',
    Peach = 'bg-[#FFDAB9]',
    PaleTurquoise = 'bg-[#AFEEEE]',
    LightCoral = 'bg-[#F08080]',
    Thistle = 'bg-[#D8BFD8]',
    Wheat = 'bg-[#F5DEB3]',
}
