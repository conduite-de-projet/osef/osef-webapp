export enum RepositoryType {
    GITLAB = 'gitlab',
    GITHUB = 'github',
    GITLAB_HOSTED = 'gitlab-hosted',
}
