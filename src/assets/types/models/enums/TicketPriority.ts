export enum TicketPriority {
    VeryLow = 'vl',
    Low = 'l',
    Medium = 'm',
    High = 'h',
    VeryHigh = 'vh',
    Highest = 'hh',
}
