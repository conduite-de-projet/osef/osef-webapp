export enum ProjectStatus {
    Active = 'active',
    Inactive = 'inactive',
    Completed = 'completed',
}
