export enum TicketType {
    USER_STORY = 'UserStory',
    EPIC = 'Epic',
    TASK = 'Task',
    BUG = 'Bug',
    SUBTASK = 'SubTask',
}
