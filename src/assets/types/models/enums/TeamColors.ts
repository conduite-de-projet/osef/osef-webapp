export default {
    LightPink: '#FFB6C1',
    LightBlue: '#ADD8E6',
    LightGreen: '#90EE90',
    LightYellow: '#FFFFE0',
    Lavender: '#E6E6FA',
    Peach: '#FFDAB9',
    PaleTurquoise: '#AFEEEE',
    LightCoral: '#F08080',
    Thistle: '#D8BFD8',
    Wheat: '#F5DEB3',
};
