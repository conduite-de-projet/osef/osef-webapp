import { OrganizationUserRole } from '@/assets/types/models/OrganizationUserRole';
import { BaseModel } from '@/assets/types/models/BaseModel';

export class Role extends BaseModel {
    name: string;
    description?: string;
    created_at?: Date;
    updated_at?: Date;
    user_roles?: Set<OrganizationUserRole>;
}
