import { Project } from '@/assets/types/models/Project';
import { BaseModel } from '@/assets/types/models/BaseModel';
import { SprintStatus } from '@/assets/types/models/enums/SprintStatus';
import { SprintBlockers } from '@/assets/types/models/enums/SprintBlockers';

export class Sprint extends BaseModel {
    id: string;
    project: Project;
    previousSprintId?: string | null;
    nextSprintId?: string | null;
    dependentSprintId?: string | null;
    name: string;
    goals: string;
    status: SprintStatus;
    startDate: string;
    actualStartDate?: string | null;
    endDate: string;
    actualEndDate?: string | null;
    velocity?: number | null;
    blockers: SprintBlockers[];
    createdAt: string;
    updatedAt: string;
}
