import { TeamUser } from '@/assets/types/models/TeamUser';
import { Organization } from '@/assets/types/models/Organization';
import TeamColors from '@/assets/types/models/enums/TeamColors';
import { BaseModel } from '@/assets/types/models/BaseModel';

export class Team extends BaseModel {
    organization: Organization;
    name: string;
    color: TeamColors;
    description?: string;
    created_at: Date;
    updated_at: Date;
    users: Set<TeamUser>;
}
