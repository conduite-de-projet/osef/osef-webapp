import { BaseModel } from '@/assets/types/models/BaseModel';
import { Ticket } from '@/assets/types/models/Ticket';
import { User } from '@/assets/types/models/User';

export class Comment extends BaseModel {
    ticket: Ticket | null;
    user: User | null;
    content: string;
    is_edited: boolean;
    created_at: Date;
    updated_at: Date;
}
