export class LoginResponse {
    id: string;
    email: string;
    firstName: string;
    lastName: string;
    status: string;
}
