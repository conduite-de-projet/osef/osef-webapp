import { Project } from '@/assets/types/models/Project';
import { Team } from '@/assets/types/models/Team';
import { BaseModel } from '@/assets/types/models/BaseModel';

export class Organization extends BaseModel {
    name: string;
    description: string;
    created_at: Date;
    updated_at: Date;
    teams: Set<Team>;
    projects: Set<Project>;
}
