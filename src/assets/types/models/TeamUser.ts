import { User } from '@/assets/types/models/User';
import { Team } from '@/assets/types/models/Team';
import { BaseModel } from '@/assets/types/models/BaseModel';

export class TeamUser extends BaseModel {
    team: Team;
    user: User;
    joined_at: Date;
}
