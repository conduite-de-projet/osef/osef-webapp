import { Ticket } from '@/assets/types/models/Ticket';
import { Label } from '@/assets/types/models/Label';

export class TicketLabel {
    ticket: Ticket | null;
    label: Label | null;
    created_at: Date;
}
