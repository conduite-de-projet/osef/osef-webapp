import { TicketLabel } from '@/assets/types/models/TicketLabel';
import { Project } from '@/assets/types/models/Project';
import { User } from '@/assets/types/models/User';
import { Sprint } from '@/assets/types/models/Sprint';
import { TicketType } from '@/assets/types/models/enums/TicketType';
import { TicketStatus } from '@/assets/types/models/enums/TicketStatus';
import { TicketPriority } from '@/assets/types/models/enums/TicketPriority';
import { BaseModel } from '@/assets/types/models/BaseModel';

export class Ticket extends BaseModel {
    project: Project | null;
    user?: User | null;
    assignee?: User | null;
    sprint?: Sprint | null;
    parent_ticket?: Ticket | null;
    type: TicketType;
    status: TicketStatus;
    title: string;
    description?: string;
    priority: TicketPriority;
    estimated_time?: number;
    time_spent?: number;
    story_points?: number;
    deadline?: Date;
    createdAt?: Date;
    updatedAt?: Date;
    labels: Set<TicketLabel>;
    comments: Set<Comment>;
    child_tickets: Set<Ticket>;
    repositoryId?: string;
    code: string;
    reference: number;
}
