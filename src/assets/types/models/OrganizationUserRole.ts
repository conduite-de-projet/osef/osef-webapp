import { Organization } from '@/assets/types/models/Organization';
import { User } from '@/assets/types/models/User';
import { Role } from '@/assets/types/models/Role';
import { BaseModel } from '@/assets/types/models/BaseModel';

export class OrganizationUserRole extends BaseModel {
    organization: Organization;
    user: User;
    role: Role;
    assigned_at: Date;
}
