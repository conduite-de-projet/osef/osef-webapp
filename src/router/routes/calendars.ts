export default {
    path: '/calendars',
    name: 'Calendars',
    component: (): Promise<typeof import('@/pages/Calendars/Index.vue')> =>
        import('@/pages/Calendars/Index.vue'),
};
