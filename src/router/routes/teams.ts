export default {
    path: 'teams',
    name: 'Teams',
    redirect: {
        name: 'TeamList',
    },
    props: true,
    component: (): Promise<typeof import('@/pages/Teams/Index.vue')> =>
        import('@/pages/Teams/Index.vue'),
    children: [
        {
            path: ':teamId',
            name: 'TeamView',
            component: (): Promise<typeof import('@/pages/Teams/View.vue')> =>
                import('@/pages/Teams/View.vue'),
            props: true,
        },
        {
            path: 'list',
            name: 'TeamList',
            component: (): Promise<typeof import('@/pages/Teams/List.vue')> =>
                import('@/pages/Teams/List.vue'),
            props: true,
        },
        {
            path: 'create',
            name: 'TeamCreate',
            component: (): Promise<typeof import('@/pages/Teams/Create.vue')> =>
                import('@/pages/Teams/Create.vue'),
            props: true,
        },
        {
            path: ':teamId/edit',
            name: 'TeamEdit',
            component: (): Promise<typeof import('@/pages/Teams/Create.vue')> =>
                import('@/pages/Teams/Create.vue'),
            props: true,
        },
    ],
};
