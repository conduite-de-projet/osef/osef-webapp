import { NavigationGuardNext, RouteLocationNormalized } from 'vue-router';
import useRepository from '../../assets/composables/useRepository';
import { Organization } from '../../assets/types/models/Organization';

async function validOrganizationId(
    to: RouteLocationNormalized,
    _from: RouteLocationNormalized,
    next: NavigationGuardNext,
): Promise<void> {
    const orgaRepo = useRepository<Organization>('organizations');
    const { organizationId } = to.params;
    try {
        await orgaRepo.getById(organizationId as string);
        next();
    } catch {
        next({ path: '/' });
    }
}

export default {
    path: '/organizations',
    name: 'Organizations',
    component: (): Promise<typeof import('@/pages/Organizations/Index.vue')> =>
        import('@/pages/Organizations/Index.vue'),
    children: [
        {
            path: 'action/join',
            name: 'JoinCreate',
            component: (): Promise<
                typeof import('@/pages/Organizations/Join.vue')
            > => import('@/pages/Organizations/Join.vue'),
        },
        {
            path: ':organizationId',
            name: 'OrganizationDashboard',
            component: (): Promise<
                typeof import('@/pages/Organizations/Dashboard.vue')
            > => import('@/pages/Organizations/Dashboard.vue'),
            props: true,
            beforeEnter: validOrganizationId,
        },
        {
            path: ':organizationId?',
            name: 'RedirectOrganization',
            props: true,
            beforeEnter: validOrganizationId,
        },
    ],
};
