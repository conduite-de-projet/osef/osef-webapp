export default {
    path: '/users',
    name: 'Users',
    component: (): Promise<typeof import('@/pages/Users/Index.vue')> =>
        import('@/pages/Users/Index.vue'),
    children: [
        {
            path: ':userId',
            name: 'UserView',
            component: (): Promise<typeof import('@/pages/Users/View.vue')> =>
                import('@/pages/Users/View.vue'),
            props: true,
        },
        {
            path: ':userId/edit',
            name: 'UserEdit',
            component: (): Promise<typeof import('@/pages/Users/Edit.vue')> =>
                import('@/pages/Users/Edit.vue'),
            props: true,
        },
    ],
};
