export default {
    path: '/settings',
    name: 'Settings',
    component: (): Promise<typeof import('@/pages/Settings/Index.vue')> =>
        import('@/pages/Settings/Index.vue'),
};
