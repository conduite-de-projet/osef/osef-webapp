export default [
    {
        path: ':projectId/sprints',
        name: 'ProjectSprints',
        component: (): Promise<
            typeof import('@/pages/Projects/Sprint/Index.vue')
        > => import('@/pages/Projects/Sprint/Index.vue'),
        props: true,
    },
    {
        path: ':projectId/sprints/create',
        name: 'ProjectSprintsCreate',
        component: (): Promise<
            typeof import('@/pages/Projects/Sprint/Create.vue')
        > => import('@/pages/Projects/Sprint/Create.vue'),
        props: true,
    },
    {
        path: ':projectId/sprints/:sprintId',
        name: 'ProjectSprintsView',
        component: (): Promise<
            typeof import('@/pages/Projects/Sprint/View.vue')
        > => import('@/pages/Projects/Sprint/View.vue'),
        props: true,
    },
    {
        path: ':projectId/sprints/:sprintId/edit',
        name: 'ProjectSprintsEdit',
        component: (): Promise<
            typeof import('@/pages/Projects/Sprint/Edit.vue')
        > => import('@/pages/Projects/Sprint/Edit.vue'),
        props: true,
    },
];
