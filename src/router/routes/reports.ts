export default {
    path: '/reports',
    name: 'Reports',
    component: (): Promise<typeof import('@/pages/Reports/Index.vue')> =>
        import('@/pages/Reports/Index.vue'),
};
