import sprints from '@/router/routes/sprints';

export default {
    path: 'projects',
    props: true,
    children: [
        {
            path: '',
            name: 'Projects',
            component: (): Promise<
                typeof import('@/pages/Projects/Index.vue')
            > => import('@/pages/Projects/Index.vue'),
            props: true,
        },
        {
            path: ':projectId',
            name: 'ProjectView',
            component: (): Promise<
                typeof import('@/pages/Projects/View.vue')
            > => import('@/pages/Projects/View.vue'),
            props: true,
        },
        {
            path: ':projectId/edit',
            name: 'ProjectEdit',
            component: (): Promise<
                typeof import('@/pages/Projects/Edit.vue')
            > => import('@/pages/Projects/Edit.vue'),
            props: true,
        },
        {
            path: 'create',
            name: 'ProjectCreate',
            component: (): Promise<
                typeof import('@/pages/Projects/Create.vue')
            > => import('@/pages/Projects/Create.vue'),
            props: true,
        },
        {
            path: ':projectId/backlog',
            name: 'ProjectBacklog',
            component: (): Promise<
                typeof import('@/pages/Projects/Backlog/Index.vue')
            > => import('@/pages/Projects/Backlog/Index.vue'),
            props: true,
        },
        {
            path: ':projectId/teams',
            name: 'ProjectTeam',
            component: (): Promise<
                typeof import('@/pages/Projects/Team/Index.vue')
            > => import('@/pages/Projects/Team/Index.vue'),
            props: true,
        },
        ...sprints,
    ],
};
