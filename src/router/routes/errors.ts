import { RouteRecordRaw } from 'vue-router';
import Forbidden from '@/pages/Errors/Forbidden.vue';
import ServerError from '@/pages/Errors/ServerError.vue';
import NotFound from '@/pages/Errors/NotFound.vue';
import TeaPot from '@/pages/Errors/TeaPot.vue';
import Unauthorized from '@/pages/Errors/Unauthorized.vue';

export const routes: RouteRecordRaw[] = [
    {
        path: '/401',
        name: 'Unauthorized',
        component: Unauthorized,
    },
    {
        path: '/403',
        name: 'Forbidden',
        component: Forbidden,
    },
    {
        path: '/418',
        name: "I'm a Teapot",
        component: TeaPot,
    },
    {
        path: '/500',
        name: 'Server Error',
        component: ServerError,
    },
    {
        path: '/:pathMatch(.*)*',
        name: 'Not Found',
        component: NotFound,
    },
];
