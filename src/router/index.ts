import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import SideBarLayout from '../layouts/SideBarLayout.vue';
import Dashboard from '../pages/Dashboard/Index.vue';
import Homepage from '../pages/Homepage/Index.vue';
import { routes as errors } from './routes/errors.ts';
import users from './routes/users.ts';
import organizations from './routes/organizations.ts';
import calendars from './routes/calendars.ts';
import reports from './routes/reports.ts';
import settings from './routes/settings.ts';
import routeExists from './middlewares/routeExists.ts';
import middlewarePipe from '../assets/helpers/middlewarePipe.ts';
import Register from '../pages/Auth/Register.vue';
import Login from '../pages/Auth/Login.vue';
import DefaultLayout from '../layouts/DefaultLayout.vue';
import AuthLayout from '../layouts/AuthLayout.vue';
import projects from '@/router/routes/projects';
import teams from '@/router/routes/teams';
import Logout from '../pages/Auth/Logout.vue';

const routes: RouteRecordRaw[] = [
    {
        path: '/',
        component: DefaultLayout,
        children: [
            { path: '', component: Homepage, name: 'Homepage' },
            ...errors,
        ],
    },
    {
        path: '/',
        component: AuthLayout,
        children: [
            { path: 'login', component: Login, name: 'Login' },
            { path: 'register', component: Register, name: 'Register' },
            { path: 'logout', component: Logout, name: 'Logout' },
        ],
    },
    {
        path: '/',
        component: SideBarLayout,
        children: [
            { path: 'dashboard', name: 'Dashboard', component: Dashboard },
            users,
            organizations,
            projects,
            teams,
            calendars,
            reports,
            settings,
        ],
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

const middlewares = [routeExists(router)];

router.beforeEach(middlewarePipe(middlewares));

export default router;
