import { Router } from 'vue-router';
import { Middleware } from '../../assets/types/middleware';

export default (router: Router): Middleware => {
    return (to, _from, next) => {
        const routeExists = router
            .getRoutes()
            .some((route) => route.name === to.name);
        if (to.name && !routeExists) {
            next({ name: 'NotFound' });
        } else {
            next();
        }
    };
};
