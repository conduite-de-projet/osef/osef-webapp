import { createApp } from 'vue';
import './style.css';
import './assets/css/toastify.css';
import App from './App.vue';
import router from './router';
import en from './assets/content/texts/en.json';
import fr from './assets/content/texts/fr.json';
import { Locales } from './assets/types/locales';
import { Locale } from './assets/types/locale';
import Vue3Toastify, { toast, type ToastContainerOptions } from 'vue3-toastify';

const app = createApp(App);

console.warn(import.meta.env);

const availableLocales: Locales = ['en', 'fr'];
let currentLocale: Locale = 'en';

app.config.globalProperties.$texts = { en, fr };
app.config.globalProperties.$locale = currentLocale;
app.config.globalProperties.$availableLocales = availableLocales;
app.config.globalProperties.$text = (
    path: string,
    replace: number | string | null = null,
): string => {
    const keys = path.split('.');
    let value: string = app.config.globalProperties.$texts[currentLocale];

    for (const key of keys) {
        if (value[key] !== undefined) {
            value = value[key];
        } else {
            console.warn(
                `Translation path "${path}" not found for locale "${currentLocale}".`,
            );
            return path;
        }
    }
    if (replace) {
        value = value.replace('{}', replace);
    }

    return value;
};
app.config.globalProperties.$switchLocale = (newLocale: Locale): void => {
    if (availableLocales.includes(newLocale)) {
        currentLocale = newLocale;
        app.config.globalProperties.$locale = newLocale;
        console.log(`Locale switched to: ${newLocale}`);
    } else {
        console.warn(`Locale "${newLocale}" is not available.`);
    }
};
app.config.globalProperties.$textExists = (path: string): boolean => {
    const keys = path.split('.');
    let value: string | null =
        app.config.globalProperties.$texts[currentLocale];

    for (const key of keys) {
        if (value[key] !== undefined) {
            value = value[key];
        } else {
            return false;
        }
    }

    return true;
};

app.use(router)
    .use(Vue3Toastify, {
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT,
    } as ToastContainerOptions)
    .mount('#app');
