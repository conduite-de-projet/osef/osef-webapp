import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'

export default defineConfig(({ mode }) => {
  const env = loadEnv(mode, process.cwd())

  return {
    plugins: [vue()],
    resolve: {
      alias: {
        '@': path.resolve(__dirname, './src'),
      },
    },
    server: {
      proxy: {
        '/api': {
          target: env.VITE_API_HOST || '*',
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/api/, ''),
        },
        '/ugitl':{
          target: 'https://gitlab.emi.u-bordeaux.fr',
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/ugitl/, ''),
        },
      },
    },
  }
})
