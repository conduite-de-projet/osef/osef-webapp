# osef-webapp

## Installation
1. Have node installed (min node 18)
2. Run the following command:
   ```shell
   cp .env.example .env
   ````
3. Fill the .env file for the api url and DO NOT COMMIT the .env file.
4. Install [lefthook](https://github.com/evilmartians/lefthook/tree/master) on your machine for commit validation
5. Run `lefthook install` to sync the hooks with your project. You can see the config in `.lefthook/commit-msg/pre-commit.sh`, the script checks that the commit respects the
conventional commit and the SEMVER.
6. Run
```shell
yarn install
```
or
```shell
npm install
```
and finally 
```shell
yarn dev
```
or 
```shell
npm run dev
```
