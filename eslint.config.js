import path from 'path';
import { fileURLToPath } from 'url';

import typescriptPlugin from '@typescript-eslint/eslint-plugin';
import typescriptParser from '@typescript-eslint/parser';
import prettierPlugin from 'eslint-plugin-prettier';

const __dirname = path.dirname(fileURLToPath(import.meta.url));

export default [
    {
        files: ['src/**/*.{js,ts}'],
        languageOptions: {
            parser: typescriptParser,
            parserOptions: {
                project: './tsconfig.eslint.json',
                tsconfigRootDir: __dirname,
                extraFileExtensions: ['vue']
            },
            ecmaVersion: 'latest',
            sourceType: 'module'
        },
        plugins: {
            '@typescript-eslint': typescriptPlugin,
            'prettier': prettierPlugin
        },
        rules: {
            ...typescriptPlugin.configs.recommended.rules,

            // Règles de bonnes pratiques pour TypeScript
            '@typescript-eslint/explicit-function-return-type': 'error',
            '@typescript-eslint/no-explicit-any': 'warn',
            '@typescript-eslint/explicit-module-boundary-types': 'warn',
            '@typescript-eslint/no-unused-vars': ['warn', { 'argsIgnorePattern': '^_' }],
            '@typescript-eslint/no-non-null-assertion': 'warn',
            '@typescript-eslint/no-inferrable-types': 'warn',

            // Autres règles de style et de consistance
            '@typescript-eslint/array-type': ['warn', { 'default': 'array-simple' }],
            '@typescript-eslint/consistent-type-assertions': 'warn',
            '@typescript-eslint/consistent-type-definitions': ['warn', 'interface'],
            '@typescript-eslint/no-empty-function': 'warn',
            '@typescript-eslint/no-empty-interface': 'warn',
            '@typescript-eslint/no-floating-promises': 'error',
            '@typescript-eslint/no-misused-promises': 'error',
            '@typescript-eslint/no-shadow': ['warn'],
            '@typescript-eslint/prefer-for-of': 'warn',
            '@typescript-eslint/prefer-optional-chain': 'warn',
            '@typescript-eslint/prefer-nullish-coalescing': ['warn'],
            '@typescript-eslint/consistent-indexed-object-style': ['warn', 'record'],
            '@typescript-eslint/prefer-ts-expect-error': 'warn',

            // Enforce Prettier
            'prettier/prettier': 'error',
            'curly': ['error', 'multi-line']
        }
    }
];
